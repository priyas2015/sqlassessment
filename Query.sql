--SELECT TOP 1 [date] FROM apple --if you store data as a string
--SELECT TOP 1 
--SUBSTRING([date],7,2)+'/'+
--SUBSTRING([date],5,2)+'/'+
--SUBSTRING([date],1,4) FROM apple


--SELECT TOP 1 getdate() FROM apple
--SELECT TOP 1
--CONVERT(varchar(20) , getdate(), 103)
--FROM apple

-- Gets the volume weighted average
--SELECT SUM(vol*[close])/SUM(vol) as VOLUME_WEIGHTED_PRICE
--FROM apple
--WHERE [date] BETWEEN '201010110900' AND '201010111400'

--SELECT
--	SUBSTRING([date],7, 2) AS day , 
--	SUBSTRING([date],5, 2) AS month ,
--	SUBSTRING([date],1, 4) AS year
--FROM apple 

-- SELECT CONVERT(DATE ,SUBSTRING([date],7, 2)+'/'+SUBSTRING([date],5, 2)+'/'+SUBSTRING([date],1, 4),103)FROM apple 
-- SELECT TOP 1 SUBSTRING([date],7, 2)+'/'+SUBSTRING([date],5, 2)+'/'+SUBSTRING([date],1, 4) from apple as day

--SELECT TOP 1 [DATE] FROM apple
--DECLARE @indate varchar(10)
--SET @indate = (SELECT TOP 1
--	SUBSTRING([date],7, 2) + '/' + 
--	SUBSTRING([date],5, 2) + '/' + 
--	SUBSTRING([date],1, 4) 
--	FROM apple)
--SELECT CONVERT(DATE, @indate, 103)

-- DECLARE @thedate varchar(14)
-- SET @thedate = '201011010900'
-- SELECT STUFF(STUFF(@thedate,0,0,' '),12,0,':')

--converts the date into a DATE format 

-- SELECT CONVERT(datetime, STUFF(STUFF([date],9,0,' '),12,0,':')) as newDate FROM apple


CREATE PROCEDURE getVWA 
@indate varchar(14),
@outdate varchar(14)
AS
	SELECT CONVERT(datetime, STUFF(STUFF(@indate,9,0,' '),12,0,':')),
	CONVERT(datetime, STUFF(STUFF(@outdate,9,0,' '),12,0,':')) ,
	SUM(vol*[close])/SUM(vol) as VOLUME_WEIGHTED_PRICE
	FROM apple
	WHERE [date] BETWEEN @indate AND @outdate
EXEC getVWA '201011010900' , '201011011400'



-- get time 
--SELECT
--	SUBSTRING([date],9, 4) AS timeOfDay
--FROM apple 

--CREATE PROCEDURE vwap
--@indate varchar(10)
--AS	
--	SELECT CONVERT(DATE, @indate, 103);

--exec vwap ''